package a02b.sol2;

import java.util.Set;

public interface Model {
    
    void advance();
    
    Set<Integer> positions();
    
}
