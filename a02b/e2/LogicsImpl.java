package a02b.e2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class LogicsImpl implements Logics {
	
	private List<Integer> stars = new ArrayList<Integer>(Arrays.asList(0,1,2));
	private int size;
	
	public LogicsImpl(int size) {
		super();
		this.size = size;
		
	}

	@Override
	public void update() {
		for(int i = 0; i < this.stars.size(); i++) {
			int newValue = this.stars.get(i)+1;
			if(newValue == this.size) {
				newValue = 0;
			}
			this.stars.set(i, newValue);
			
		}
//		this.stars = this.stars.stream().map(i -> (i +1) % size).collect(Collectors.toCollection(ArrayList::new));
	}




	@Override
	public List<Integer> getValues() {
		return Collections.unmodifiableList(this.stars);
	}


}
