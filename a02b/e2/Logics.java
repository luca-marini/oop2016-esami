package a02b.e2;

import java.util.List;
import java.util.Set;

public interface Logics {

	void update();
	
	List<Integer> getValues();

}
