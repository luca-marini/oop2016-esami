package a02b.e2;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.swing.*;

public class GUI extends JFrame {

    private JButton jbValue1;
    private JButton jbValue2;
    private List<JButton> jbs = new ArrayList<>();
    
    
	public GUI(int size){
		
		
		JPanel jp = new JPanel();
	
		jbs = Stream.generate(() -> new JButton(" ")).limit(size)
													 .peek(b -> jp.add(b))
													 .peek(b -> b.setEnabled(false))
													 .collect(Collectors.toCollection(ArrayList::new));
		final Agent agent = new Agent(size);
		

        JFrame jf = new JFrame();
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);		
        agent.start();
	}
	
	private class Agent extends Thread {

		private volatile boolean stop = false;
		private Logics logic;
	
		private Agent(int size) {
			this.logic = new LogicsImpl(size);
		}
		void viewUpdate(List<Integer> positions){
			jbs.forEach(jb -> jb.setText(positions.contains(jbs.indexOf(jb))?"*":" "));
		}
		
		public void run() {
			while (!stop) {
			    List<Integer> positions = this.logic.getValues();
			    SwingUtilities.invokeLater(()->viewUpdate(positions));

			    logic.update();
				try {
					Thread.sleep(300);
				} catch (Exception ex) {
				}
			}
		}

		public void stopAdvancing() {
			this.stop = true;
		}
	}
	

}
