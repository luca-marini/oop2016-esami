package a02b.e1;

public class LimitedDevice implements Device {

	private boolean isOn = false;
	private int nSwitch = 0;
	private int count;	
	
	public LimitedDevice(int count) {
		super();
		this.count = count;
	}

	@Override
	public void on() {
		this.nSwitch++;
		if(isWorking()) {
			this.isOn = true;
		} else this.isOn = false;
	}

	@Override
	public void off() {
		this.isOn = false;
	}

	@Override
	public boolean isOn() {
		return this.isOn;
	}

	@Override
	public boolean isWorking() {
		return this.nSwitch < count;
	}

}
