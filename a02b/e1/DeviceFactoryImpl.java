package a02b.e1;

public class DeviceFactoryImpl implements DeviceFactory {

	@Override
	public Device createDeviceNeverFailing() {
		return new UnlimitedDevice();
	}

	@Override
	public Device createDeviceFailingAtSwitchOn(int count) {
		return new LimitedDevice(count);
	}

	@Override
	public LuminousDevice createLuminousDeviceFailingAtMaxIntensity(int maxIntensity) {
		return new LimitedMaxIntensityDevice(maxIntensity);
	}

}
