package a02b.e1;

public class UnlimitedDevice implements Device {

	private boolean isOn = false;
	
	@Override
	public void on() {
		this.isOn = true;
	}

	@Override
	public void off() {
		this.isOn = false;
	}

	@Override
	public boolean isOn() {
		return this.isOn;
	}

	@Override
	public boolean isWorking() {
		return true;
	}

}
