package a02b.e1;

public class LimitedMaxIntensityDevice implements LuminousDevice {

	private boolean isOn = false;
	private int nSwitch = 0;
	private int maxBrighten;
	private int brighteness = 0;
	private boolean last = false;
	
	
	public LimitedMaxIntensityDevice(int maxBrighten) {
		super();
		this.maxBrighten = maxBrighten;
	}

	@Override
	public void on() {
		if(this.brighteness == maxBrighten || this.last) {
			this.isOn = false;
			this.last = true;
		} else { 
			this.isOn = true;
		}
	}

	@Override
	public void off() {
		this.isOn = false;
	}

	@Override
	public boolean isOn() {
		if(this.isOn && this.brighteness == maxBrighten) {
			return false;
		}
		return this.isOn;
	}

	@Override
	public boolean isWorking() {
		if(this.last) {
			return false;
		}
		return this.brighteness < maxBrighten || (this.brighteness == maxBrighten && this.isOn == false && this.last == false);
	}

	@Override
	public void dim() {
		this.brighteness--;
	}

	@Override
	public void brighten() {
		this.brighteness++;
	}

	@Override
	public int getIntesity() {
		return this.brighteness;
	}

}
