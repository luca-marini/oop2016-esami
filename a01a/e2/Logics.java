package a01a.e2;

import java.io.FileNotFoundException;

public interface Logics {

	void zero();
	
	void rand();
	
	void dec();
	
	void print() throws FileNotFoundException;
	
}
