package a01a.e2;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;

public class GUI {
    
	private Logics logic;
	
    public GUI(String fileName) throws IOException{
    	this.logic = new LogicsImpl(fileName);
        JFrame jf = new JFrame();
        JButton rand = new JButton("RAND");
        JButton dec = new JButton("DEC");
        JButton zero = new JButton("ZERO");
        JButton ok = new JButton("OK");
        rand.addActionListener(e -> {
        	logic.rand();
        	
        });
        
        dec.addActionListener(e -> {
        	
        	logic.dec();
        });
   
   		zero.addActionListener(e -> {
   			logic.zero();
   	
   		});
        
        ok.addActionListener(e -> {
        	try {
				logic.print();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
        });
        
        
        
        JPanel jp = new JPanel();
        jp.add(rand);
        jp.add(dec);
        jp.add(zero);
        jp.add(ok);
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
    }

}
