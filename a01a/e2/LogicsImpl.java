package a01a.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class LogicsImpl implements Logics {

	private Random r = new Random();
	private int c = 0;
	private PrintStream ps;
	private List<Integer> print = new ArrayList<>();
	private String fileName;
 	
	
	public LogicsImpl(String fileName){
		this.fileName = Objects.requireNonNull(fileName);
	}

	@Override
	public void zero() {
		print.add(0);
	}

	@Override
	public void rand() {
		print.add(r.nextInt(10) + 1);
	}

	@Override
	public void dec() {
		c--;
		print.add(c);
	}

	@Override
	public void print() throws FileNotFoundException {
		ps = new PrintStream(fileName);
		System.out.println(this.print);
		ps.println(this.print);
		this.print.clear();
		this.c = 0;
	}

}
