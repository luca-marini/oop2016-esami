package a01a.e1;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

public class SequenceImpl<X> implements Sequence<X> {

	private List<X> elements = new ArrayList<>();
	
	
	
	public SequenceImpl(List<X> elements) {
		super();
		this.elements = elements;
	}

	@Override
	public Optional<X> getAtPosition(int position) {
		if(position >= 0 && position < this.elements.size()) {
			return Optional.of(this.elements.get(position));
		}
		return Optional.empty();
	}

	@Override
	public int size() {
		return this.elements.size();
	}

	@Override
	public List<X> asList() {
		return this.elements;
	}

	@Override
	public void executeOnAllElements(Executor<X> executor) {
		IntStream.range(0, this.elements.size()).forEach(i -> executor.execute(this.elements.get(i)));
	}

}
