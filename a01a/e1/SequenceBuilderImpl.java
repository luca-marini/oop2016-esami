package a01a.e1;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

public class SequenceBuilderImpl<X> implements SequenceBuilder<X> {

	private List<X> elements = new ArrayList<>();
	private boolean isBuild = false;
	
	@Override
	public void addElement(X x) {
		this.elements.add(x);
	}

	@Override
	public void removeElement(int position) {
		this.elements.remove(position);
	}

	@Override
	public void reverse() {
		List<X> copy = new ArrayList<>();
		for(int i = this.elements.size() - 1; i >= 0; i--) {
			copy.add(this.elements.get(i));
		}
		IntStream.range(0, copy.size()).forEach(i -> this.elements.remove(0));
		this.elements.addAll(copy);
	}

	@Override
	public void clear() {
		IntStream.range(0, this.elements.size()).forEach(i -> this.elements.remove(0));
	}
		

	@Override
	public Optional<Sequence<X>> build() {
		if(!isBuild) {
			this.isBuild = true;
			return Optional.of(new SequenceImpl<X>(this.elements));
		}
		return Optional.empty();
	}

	@Override
	public Optional<Sequence<X>> buildWithFilter(Filter<X> filter) {
		for(X e : this.elements) {
			if(!filter.check(e)) {
				return Optional.empty();
			}
		}
		return build();
	}

	@Override
	public <Y> SequenceBuilder<Y> mapToNewBuilder(Mapper<X, Y> mapper) {
		SequenceBuilderImpl<Y> seqy = new SequenceBuilderImpl<Y>();
		for(X e : this.elements) {
			seqy.addElement(mapper.transform(e));
		}
		return seqy;
	}

}
