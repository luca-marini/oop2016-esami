package a03a.e1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;

public class BagFactoryImpl implements BagFactory {

	@Override
	public <X> Bag<X> empty() {
		return new Bag<X>() {

			@Override
			public int numberOfCopies(X x) {
				return 0;
			}

			@Override
			public int size() {
				return 0;
			}

			@Override
			public List<X> toList() {
				return Collections.emptyList();
			}
		};
	}

	@Override
	public <X> Bag<X> fromSet(Set<X> set) {
		return new Bag<X>() {

			@Override
			public int numberOfCopies(X x) {
				return set.contains(x) ? 1 : 0;
			}

			@Override
			public int size() {
				return set.size();
			}

			@Override
			public List<X> toList() {
				List<X> list = new ArrayList<X>();
				list.addAll(set);
				return Collections.unmodifiableList(list);
			}
		};
	}

	@Override
	public <X> Bag<X> fromList(List<X> list) {
		return new Bag<X>() {

			@Override
			public int numberOfCopies(X x) {
				return (int) list.stream().filter(e -> e.equals(x)).count();
			}

			@Override
			public int size() {
				return list.size();
			}

			@Override
			public List<X> toList() {
				return Collections.unmodifiableList(list);
			}
		};
	}

	@Override
	public <X> Bag<X> bySupplier(Supplier<X> supplier, int nElements, ToIntFunction<X> copies) {
		return new Bag<X>() {
			
			private X x;

			@Override
			public int numberOfCopies(X x) {
				this.x = x;
				return copies.applyAsInt(x);
			}

			@Override
			public int size() {
				return nElements * copies.applyAsInt(x);
			}

			@Override
			public List<X> toList() {
				List<X> list = new ArrayList<X>();
				for(int i = 0; i < size(); i+=numberOfCopies(x)) {
					X e = supplier.get();
					for(int j = 0; j < numberOfCopies(x); j++) {
					list.add(e);
					}
				}
				return Collections.unmodifiableList(list);
			}
		};
	}

	@Override
	public <X> Bag<X> byIteration(X first, UnaryOperator<X> next, int nElements, ToIntFunction<X> copies) {
		return new Bag<X>() {

			private X x;
			
			@Override
			public int numberOfCopies(X x) {
				this.x = x;
				return copies.applyAsInt(x);
			}

			@Override
			public int size() {
				return nElements * copies.applyAsInt(x);
			}

			@Override
			public List<X> toList() {
				List<X> list = new ArrayList<X>();
				list.add(first);
				
				for(int i = 0; i < size(); i++) {
					X e = list.get(i);
					list.add(next.apply(e));
				}
				return Collections.unmodifiableList(list);
			}
		};
	}

}
