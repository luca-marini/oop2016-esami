package a03a.e2;

public interface Logics {

	void hitA();
	
	void hitB();
	
	int getA();
	
	int getB();
	
	boolean checkFinish();
	
	void reset();
}
