package a03a.e2;

public class LogicsImpl implements Logics {
	
	private int A;
	private int B;
	private int size;
	
	

	public LogicsImpl(int size) {
		super();
		this.size = size;
		this.reset();
	}

	@Override
	public void hitA() {
		if(this.getA() < this.getB() - 1) {
			this.A++;
		}
	}

	@Override
	public void hitB() {
		if(this.getB() < size - 1) {
			this.B++;
		}
		
	}

	@Override
	public int getA() {
		return this.A;
	}

	@Override
	public int getB() {
		return this.B;
	}

	@Override
	public boolean checkFinish() {
		return (getA() == size - 2) && (getB() == size - 1);
	}

	@Override
	public void reset() {
		this.A = 0;
		this.B = 1;
	}

}
