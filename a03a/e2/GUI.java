package a03a.e2;

import javax.swing.*;
import java.util.*;
import java.util.stream.*;

public class GUI {
    private Logics logic;
    private List<JButton> jbs;
    private int size;
    
    void updateView() {
//    	jbs.stream().forEach(b -> b.setText(" "));
//    	jbs.get(this.logic.getA()).setText("A");
//    	jbs.get(this.logic.getB()).setText("B");
    	this.jbs.stream()
    			.forEach(b ->  b.setText(jbs.indexOf(b) == this.logic.getA() ? "A" 
    					: jbs.indexOf(b) == this.logic.getB() ? "B" : " "));
    	if(this.logic.checkFinish()) {
    		System.exit(0);
    	}
    }
    
    public GUI(int size){
    	this.size = size;
    	JPanel jp = new JPanel();
    	this.logic = new LogicsImpl(size);
    	this.jbs = Stream.generate(() -> new JButton(" "))
    					 .limit(size)
    					 .peek(jp::add)
    					 .collect(Collectors.toCollection(ArrayList::new));
    	jbs.get(this.logic.getA()).setText("A");
    	jbs.get(this.logic.getB()).setText("B");
    	
        JButton moveA = new JButton("MoveA");
        JButton moveB = new JButton("MoveB");
        JButton reset = new JButton("Reset");
        moveA.addActionListener(e -> {
        	this.logic.hitA();
        	this.updateView();
        });
        moveB.addActionListener(e -> {
        	this.logic.hitB();
        	this.updateView();
        });
        reset.addActionListener(e -> {
        	this.logic.reset();
        	this.updateView();
        });
        
        jp.add(moveA);
        jp.add(moveB);
        jp.add(reset);
        JFrame jf = new JFrame();
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
    }
    


}
