package a01b.e1;

import java.util.ArrayList;
import java.util.List;

public class BuilderWithSize<X> implements ListBuilder<X> {

	
	private List<X> elements = new ArrayList<>();
	private boolean isBuilt = false;
	private int size;
	
	public BuilderWithSize(int size) {
		super();
		this.size = size;
	}

	@Override
	public void addElement(X x) {
		if(isBuilt) {
			throw new IllegalStateException();
		} 
		this.elements.add(x);
		
	}

	@Override
	public List<X> build() {
		if(elements.size() < this.size || this.isBuilt) {
			throw new IllegalStateException();
		} 
		this.isBuilt = true;
		return this.elements;
	}

}
