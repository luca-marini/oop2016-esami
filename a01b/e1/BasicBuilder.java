package a01b.e1;

import java.util.ArrayList;
import java.util.List;

public class BasicBuilder<X> implements ListBuilder<X> {

	private List<X> elements = new ArrayList<>();
	private boolean isBuilt = false;
	
	@Override
	public void addElement(X x) {
		if(this.isBuilt) {
			throw new IllegalStateException();
		} 
		this.elements.add(x);
	}

	@Override
	public List<X> build() {
		if(!this.isBuilt) {
			this.isBuilt = true;
			return this.elements;
		}
		throw new IllegalStateException();
	}

}
