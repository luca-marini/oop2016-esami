package a01b.e1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BuilderFromElementAndWithSize<X> implements ListBuilder<X> {

	private List<X> elements = new ArrayList<>();
	private boolean isBuilt = false;
	private Collection<X> from;
	private int size;
	
	
	public BuilderFromElementAndWithSize(int size, Collection<X> from) {
		super();
		this.from = from;
		this.size = size;
	}
	@Override
	public void addElement(X x) {
		if(this.isBuilt) {
			throw new IllegalStateException();
		}
		if(!this.from.contains(x)) {
			throw new IllegalArgumentException();
		}
		this.elements.add(x);		
	}
	@Override
	public List<X> build() {
		
		if(this.isBuilt || elements.size() < this.size || this.isBuilt)  {
			throw new IllegalStateException();
		}
		this.isBuilt = true;
		return this.elements;
	}
}
