package a01b.e1;

import java.util.Collection;
import java.util.List;

public class BuildersImpl implements Builders {

	@Override
	public <X> ListBuilder<X> makeBasicBuilder() {
		return new BasicBuilder<X>();
	}

	@Override
	public <X> ListBuilder<X> makeBuilderWithSize(int size) {
		return new BuilderWithSize<X>(size);
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElements(Collection<X> from) {
		return new BuilderFromElements<X>(from);
	}

	@Override
	public <X> ListBuilder<X> makeBuilderFromElementsAndWithSize(Collection<X> from, int size) {
		return new BuilderFromElementAndWithSize<X>(size, from);
	}



}
