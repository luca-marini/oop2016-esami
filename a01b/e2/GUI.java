package a01b.e2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.swing.*;

public class GUI {
    
	private Logics logic;
	private List<JButton> jbs;
	
    public GUI(String fileName, int size) throws IOException{
    	logic = new LogicsImpl(fileName, size);
        JFrame jf = new JFrame();
        JPanel jp = new JPanel();
        
        jbs = Stream.generate(() -> new JButton()).limit(size)
        										  .peek(b -> jp.add(b))
        										  .peek(b -> b.setEnabled(false))
        										  .peek(b -> b.addActionListener(e -> {
        											  if(((JButton)e.getSource()).isEnabled()) {
        												  if(this.logic.isAllEnabled()) {
        													  try {
																this.logic.restart();
															} catch (IOException e1) {
																e1.printStackTrace();
															}
        													  this.jbs.get(this.logic.getNext()).setEnabled(true);
        												  }
        												  this.jbs.get(this.logic.getNext()).setEnabled(true);
        											  }
        										  }))
        										  .collect(Collectors.toList());
        
        this.jbs.get(this.logic.getNext()).setEnabled(true);
        
        IntStream.range(0, size).forEach(i -> jbs.get(i).setText(String.valueOf(i)));
        
        JButton reset = new JButton("Reset");
        reset.addActionListener(e -> {
        	IntStream.range(0, size).forEach(i -> jbs.get(i).setEnabled(false));
        	try {
				this.logic.restart();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        	this.jbs.get(this.logic.getNext()).setEnabled(true);
        });
        
    
        jp.add(reset);
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
    }

}
