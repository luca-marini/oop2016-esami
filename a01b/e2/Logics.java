package a01b.e2;

import java.io.IOException;

public interface Logics {
	
	void restart() throws IOException;
	
	int getNext();
	
	boolean isAllEnabled();

}
