package a01b.e2;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class LogicsImpl implements Logics {

	private int current;
	private int enabled;
	private int size;
	private List<String> lines;
	private String fileName;
	
	
	public LogicsImpl(String fileName, int size) throws IOException {
		super();
		this.lines = Files.lines(Paths.get(fileName)).collect(Collectors.toCollection(ArrayList::new));
		this.current = Integer.parseInt(lines.get(0));
		this.enabled = 0;
		this.size = size;
		this.fileName = fileName;
	}

	@Override
	public void restart() throws IOException {
		
			this.current = 0;
			this.lines.clear();
			this.lines = Files.lines(Paths.get(this.fileName)).collect(Collectors.toCollection(ArrayList::new));
		
	}

	@Override
	public int getNext() {
//		current++;
		return Integer.parseInt(this.lines.get(current++));
	}

	@Override
	public boolean isAllEnabled() {
		return this.current == this.lines.size();
	}

}
