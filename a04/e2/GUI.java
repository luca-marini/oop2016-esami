package a04.e2;

import javax.swing.*;

import sun.security.krb5.Confounder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class GUI {
    
    private JButton l = new JButton("l");
    private JButton r = new JButton("r");
    private JComboBox<String> box;
    private String confront;
    private Logics logic;
    
    public GUI(){
    	logic = new LogicsImpl();
        box = new JComboBox<>();
        box.addItem("[l,l,l,l,l]");
        box.addItem("[r,r,r]");
        box.addItem("[l,r,l,r]");
        confront = ((String)(box.getSelectedItem()));
        this.logic.setList(confront);
        ActionListener al = (e -> {
        	this.logic.reset();
        	confront = ((String)((JComboBox<?>)e.getSource()).getSelectedItem());
        	this.logic.setList(confront);
            
            System.out.println(this.logic.getList());
            
        });
        
        box.addActionListener(al);
        l.addActionListener(e -> {
        	if(!this.logic.hitFirst()) {
        		this.logic.setList(confront);
        	}
        });
        r.addActionListener(e -> {
        	if(!this.logic.hitSecond()) {
        		this.logic.setList(confront);
        	}
        });

        JPanel jp = new JPanel();
        jp.add(box);
        jp.add(l);
        jp.add(r);
        JFrame jf = new JFrame();
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
        
    }


}
