package a04.e2;

import java.util.List;

public interface Logics {
	
	boolean hitFirst();
	
	boolean hitSecond();
	
	void finish();
	
	void reset();
	
	void setList(String s);

	List<Integer> getList();
}
