package a04.e2;

import java.util.ArrayList;
import java.util.List;

public class LogicsImpl implements Logics {
	
	private List<Integer> val = new ArrayList<Integer>();
	private int c = 0;
	
	public void setList(String s){
		for(int i = 0; i < s.length() - 1; i++) {
			if(s.charAt(i) == 'l') {
				val.add(1);
			} else if(s.charAt(i) == 'r') {
				val.add(0);
			}
		}
	}

	@Override
	public boolean hitFirst() {
		if(this.val.get(c) != 1) {
			this.reset();
			return false;
		}
		if(this.c == this.val.size()-1) {
			finish();
		}
		this.c++;
		return true;
	}

	@Override
	public boolean hitSecond() {
		if(this.val.get(c) != 0){
			this.reset();
			return false;
		}
		if(this.c == this.val.size()-1) {
			finish();
		}
		this.c++;
		return true;
	}

	@Override
	public void finish() {
		System.exit(0);
	}

	@Override
	public void reset() {
		this.c = 0;
		this.val.clear();
	}

	@Override
	public List<Integer> getList() {
		return this.val;
	}

}
