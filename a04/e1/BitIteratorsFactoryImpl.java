package a04.e1;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class BitIteratorsFactoryImpl implements BitIteratorsFactory {

	@Override
	public Iterator<Bit> empty() {
		return new Iterator<BitIteratorsFactory.Bit>() {

			@Override
			public boolean hasNext() {
				return false;
			}

			@Override
			public Bit next() {
				throw new NoSuchElementException();
			}
		};
	}

	@Override
	public Iterator<Bit> zeros() {
		return new Iterator<BitIteratorsFactory.Bit>() {
			
			@Override
			public Bit next() {
				return Bit.ZERO;
			}
			
			@Override
			public boolean hasNext() {
				return true;
			}
		};
	}

	@Override
	public Iterator<Bit> ones() {
		return new Iterator<BitIteratorsFactory.Bit>() {
			
			@Override
			public Bit next() {
				return Bit.ONE;
			}
			
			@Override
			public boolean hasNext() {
				return true;
			}
		};
	}

	   /**
     * @return an iterator producing 8 bits out of an integer in [0,255], starting from the least significative (LSB)
     * for instance: 127 produces ONE,ONE,ONE,ONE,ONE,ONE,ONE,ZERO
     * suggestion: 
     * - b % 2 gives the first bit, then do: b = b/2
     * - b % 2 now gives the second bit, then do: b = b/2
     * - b % 2 now gives the third bit,.. and so on
     */
	@Override
	public Iterator<Bit> fromByteStartingWithLSB(int b) {
		return new Iterator<BitIteratorsFactory.Bit>() {
			
			private int c = 0;
			private final int BYTES = 8;
			private Bit next;
			private int bi = b;
			
			
			@Override
			public Bit next() {
					if(hasNext()) {
					next = this.bi%2 == 1 ? Bit.ONE : Bit.ZERO;
					this.bi = this.bi/2;
					this.c++;
					return next;
				} else {
					throw new NoSuchElementException();
				}
			}
			
			@Override
			public boolean hasNext() {
				return c < BYTES;
			}
		};
	}

	@Override
	public Iterator<Bit> fromBitList(List<Bit> list) {
		return list.iterator();
	}

	@Override
	public Iterator<Bit> fromBooleanList(List<Boolean> list) {
		return new Iterator<BitIteratorsFactory.Bit>() {

			private Iterator<Boolean> it = list.iterator();
			@Override
			public boolean hasNext() {
				return it.hasNext();
			}

			@Override
			public Bit next() {
				return it.next() == true ? Bit.ONE : Bit.ZERO;
			}
		};
	}

}
