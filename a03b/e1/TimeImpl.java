package a03b.e1;

public class TimeImpl implements Time {

	private int hour;
	private int min;
	private int sec;
	private int MAXHOUR = 23;
	private int MAXMIN = 59;
	private int MAXSEC = 59;
	
	
	public TimeImpl(int hour, int min, int sec) {
		super();
		if(hour > MAXHOUR || min > MAXMIN || sec > MAXSEC || hour < 0 || min < 0 || sec < 0) {
			throw new IllegalArgumentException();
		}
		this.hour = hour;
		this.min = min;
		this.sec = sec;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + MAXHOUR;
		result = prime * result + MAXMIN;
		result = prime * result + MAXSEC;
		result = prime * result + hour;
		result = prime * result + min;
		result = prime * result + sec;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeImpl other = (TimeImpl) obj;
		if (hour != other.hour)
			return false;
		if (min != other.min)
			return false;
		if (sec != other.sec)
			return false;
		return true;
	}



	@Override
	public int getHours() {
		return this.hour;
	}

	@Override
	public int getMinutes() {
		return this.min;
	}

	@Override
	public int getSeconds() {
		return this.sec;
	}

	@Override
	public String getLabel24() {
		return (this.hour < 10 ? "0" + String.valueOf(this.hour):String.valueOf(this.hour)) +
				":" + (this.min < 10 ? "0" + String.valueOf(this.min):String.valueOf(this.min)) +
				":" + (this.sec < 10 ? "0" + String.valueOf(this.sec):String.valueOf(this.sec));
	}

	@Override
	public int getSecondsFromMidnight() {
		return (this.hour * 3600) + (this.min * 60) + this.sec;
	}

}
