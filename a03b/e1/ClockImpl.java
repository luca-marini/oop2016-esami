package a03b.e1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class ClockImpl implements Clock {

	private Time time;
	private final int ONE = 1;
	private final int ZERO = 0;
	private int MAXHOUR = 23;
	private int MAXMIN = 59;
	private int MAXSEC = 59;
	
	private Map<Time, List<Runnable>> alarms = new HashMap<Time, List<Runnable>>();
	
	
	public ClockImpl(Time time) {
		super();
		this.time = time;
	}

	@Override
	public Time getTime() {
		return this.time;
	}

	@Override
	public void tick() {
		
		if(this.time.getHours() == 23 && this.time.getMinutes() == 59 && this.time.getSeconds() == 59)  {
			this.time = new TimeImpl(ZERO, ZERO, ZERO);
		} else if(this.time.getMinutes() == 59 && this.time.getSeconds() == 59) {
			this.time = new TimeImpl(this.time.getHours() + ONE, ZERO, ZERO);
		} else if(this.time.getSeconds() == 59) {
			this.time = new TimeImpl(this.time.getHours(), this.time.getMinutes() + ONE, ZERO);
		} else {
			this.time = new TimeImpl(this.time.getHours(), this.time.getMinutes(), this.time.getSeconds() + ONE);
		}
		this.alarms.entrySet().stream()
		  .filter(e -> e.getKey().equals(this.getTime()))
		  .forEach(e -> e.getValue().run());
	}

	@Override
	public void registerAlarmObserver(Time time, Runnable observer) {
		this.alarms.put(time, observer);
	}

	@Override
	public void registerHoursDeadlineObserver(int hours, Runnable observer) {
		Clock plusHpour = new ClockImpl(this.getTime());
		IntStream.range(0, hours * (MAXMIN + 2)).forEach(i -> plusHpour.tick());
		this.alarms.put(plusHpour.getTime(), this.alarms.get(plusHpour.getTime()).add(observer));
	}

	@Override
	public void registerMinutesDeadlineObserver(int minutes, Runnable observer) {
		Clock plusMin = new ClockImpl(this.getTime());
		IntStream.range(0, minutes * (MAXSEC + 2)).forEach(i -> plusMin.tick());
		this.alarms.put(plusMin.getTime(), this.alarms.get(plusMin.getTime()).add(observer));
	}

	@Override
	public void registerSecondsDeadlineObserver(int seconds, Runnable observer) {
		Clock plusSec = new ClockImpl(this.getTime());
		IntStream.range(0, seconds).forEach(i -> plusSec.tick());
		this.alarms.put(plusSec.getTime(), this.alarms.get(plusSec.getTime()).add(observer));
	}

}
