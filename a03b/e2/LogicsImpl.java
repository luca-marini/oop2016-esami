package a03b.e2;

public class LogicsImpl implements Logics {

	private int size;
	private int currentPos;
	private boolean advance = true;
	
	public LogicsImpl(int size) {
		super();
		this.size = size;
		this.currentPos = 0;
	}

	@Override
	public void start() {
		if(this.currentPos < this.size - 1 && this.advance == true) {
			this.currentPos++;
			if(this.currentPos == 9) {
				this.advance = false;
			}
		} else {
			this.currentPos--;
			if(this.currentPos == 0) {
				this.finish();
			}
		}
	}

	@Override
	public void finish() {
		System.exit(0);
	}

	@Override
	public int getCurrent() {
		return this.currentPos;
	}

}
