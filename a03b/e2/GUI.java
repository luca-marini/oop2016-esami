package a03b.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.swing.*;

public class GUI extends JFrame {

  
	private List<JButton> jbs;
	private Logics logic;
    
    void updateView() {
    	this.jbs.stream().forEach(b -> b.setText(" "));
    	this.jbs.get(this.logic.getCurrent()).setText("X");
    }
    
	public GUI(int size){
		this.logic = new LogicsImpl(size);
		JPanel jp = new JPanel();
		jbs = Stream.generate(() -> new JButton(" ")).limit(size)
											   .peek(jp::add)
											   .peek(b -> b.setEnabled(false))
											   .collect(Collectors.toList());
		final Agent agent = new Agent();									   
		this.updateView();
    	this.jbs.get(this.logic.getCurrent()).setEnabled(true);
		this.jbs.get(this.logic.getCurrent()).addActionListener(e -> {
			((JButton)e.getSource()).setEnabled(false);
			 agent.start();
		});
		
        JFrame jf = new JFrame();
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);		
       
	}
	
	private class Agent extends Thread {
	  
		private volatile boolean stop = false;
		
		public void run() {
			while (!stop) {
			    logic.start();
			   
			    SwingUtilities.invokeLater(()->{
			    	updateView();
			    	
			    });
				try {
					Thread.sleep(300);
				} catch (Exception ex) {
				}
			}
		}

		public void stopAdvancing() {
			this.stop = true;
		}
	}
	
}
