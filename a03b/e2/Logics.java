package a03b.e2;

public interface Logics {
	
	void start();
	
	void finish();
	
	int getCurrent();

}
