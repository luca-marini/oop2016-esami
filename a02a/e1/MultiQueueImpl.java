package a02a.e1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class MultiQueueImpl<T, Q> implements MultiQueue<T, Q> {

	private final Map<Q, List<T>> map = new HashMap<>();
	
	@Override
	public Set<Q> availableQueues() {
		return map.keySet();
	}

	@Override
	public void openNewQueue(Q queue) {
		if(this.map.containsKey(queue)) {
			throw new IllegalArgumentException();
		}
		this.map.put(queue, new ArrayList<>());
	}

	@Override
	public boolean isQueueEmpty(Q queue) {
		if(!this.map.containsKey(queue)) {
			throw new IllegalArgumentException();
		}
		return this.map.get(queue).isEmpty();
	}

	@Override
	public void enqueue(T elem, Q queue) {
		if(!this.map.containsKey(queue)) {
			throw new IllegalArgumentException();
		}
		this.map.get(queue).add(elem);
	}

	@Override
	public Optional<T> dequeue(Q queue) {
		if(!this.map.containsKey(queue)) {
			throw new IllegalArgumentException();
		}
		return this.map.get(queue).isEmpty() ? Optional.empty() :Optional.of(this.map.get(queue).remove(0));
	}

	@Override
	public Map<Q, Optional<T>> dequeueOneFromAllQueues() {
		Map<Q, Optional<T>> dequeue = new HashMap<>();
		this.map.entrySet().stream()
						   .forEach(e -> dequeue.put(e.getKey(), dequeue(e.getKey())));
						          
		return Collections.unmodifiableMap(dequeue);
						
	}						  

	@Override
	public Set<T> allEnqueuedElements() {
		Set<T> allEnqueued = new HashSet<>();
		this.map.entrySet().stream()
						   .forEach(e -> allEnqueued.addAll(e.getValue()));
		return allEnqueued;
	}

	@Override
	public List<T> dequeueAllFromQueue(Q queue) {
		if(!this.map.containsKey(queue)) {
			throw new IllegalArgumentException();
		}
		List<T> enqueue = new ArrayList<>();
		this.map.entrySet().stream()
						   .filter(e -> e.getKey().equals(queue))
						   .forEach(e -> enqueue.addAll(e.getValue()));
		
		this.map.entrySet().stream()
						   .filter(e -> e.getKey().equals(queue))
						   .forEach(e -> e.getValue().clear());
		return enqueue;
	}

	@Override
	public void closeQueueAndReallocate(Q queue) {
		if(map.size() == 1) {
			throw new IllegalStateException();
		}
		if(!this.map.containsKey(queue)) {
			throw new IllegalArgumentException();
		}
		List<T> moveElem = new ArrayList<>();
		this.map.entrySet().stream()
						   .filter(e -> e.getKey().equals(queue))
						   .forEach(e -> moveElem.addAll(e.getValue()));
		this.map.remove(queue);		
		this.map.entrySet().stream()
						   .findFirst()
						   .get()
						   .getValue()
						   .addAll(moveElem);						   
		
	}

}
