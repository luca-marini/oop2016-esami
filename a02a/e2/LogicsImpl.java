package a02a.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogicsImpl implements Logics {
	
	private int currentPos;
	private List<Boolean> posEnable = new ArrayList<>();
	private int size;
	private boolean right = true;
	private boolean left = false;
	

	public LogicsImpl(int size) {
		super();
		this.size = size;
		this.reset();
	}
	
	public int getCurrent() {
		return this.currentPos;
	}

	@Override
	public void move() {
		if(this.right) {
			moveRight();
		} 
		if(this.left) {
			moveLeft();
		} else {
			return ;
		}
	}

	@Override
	public void moveRight() {
		
		if(this.currentPos + 1 >= this.posEnable.size() || !this.posEnable.get(this.currentPos + 1)) {
			this.right = false;
			this.left = true;
		} else {
			this.currentPos++;
		}
		
	}

	@Override
	public void moveLeft() {
		if(this.currentPos - 1 < 0 || !this.posEnable.get(this.currentPos - 1)) {
			this.right = true;
			this.left = false;
			this.moveRight();
		} else {
			this.currentPos--;
		}
	}

	@Override
	public void hit(int pos) {
		if(this.currentPos != pos) {
			this.posEnable.set(pos, false);
		}
	}

	@Override
	public void reset() {
		this.currentPos = 0;
		this.right = true;
		this.left = false;
		this.posEnable = Stream.generate(() -> posEnable.add(true)).limit(size).collect(Collectors.toCollection(ArrayList::new));
	}

	@Override
	public boolean isEnabled(int pos) {
		return this.posEnable.get(pos);
	}

}
