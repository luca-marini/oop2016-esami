package a02a.e2;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.stream.*;

public class GUI {
    private Logics logic;
    private List<JButton> jbs = new ArrayList<>();
    public GUI(int size){
    	this.logic = new LogicsImpl(size);
    	JPanel jp = new JPanel();
    	ActionListener ac = ( e-> {
    		this.logic.hit(jbs.indexOf((JButton)e.getSource()));
    		((JButton)e.getSource()).setEnabled(this.logic.isEnabled(jbs.indexOf((JButton)e.getSource())));
    	});
    	
    	jbs = Stream.generate(() -> new JButton()).limit(size)
    										.peek(b -> b.addActionListener(ac))
    										.peek(b -> jp.add(b))
    										.peek(b -> b.setText(" "))
    										.collect(Collectors.toCollection(ArrayList::new));
    	this.jbs.get(this.logic.getCurrent()).setText("*");
        JButton move = new JButton("Move");
        JButton reset = new JButton("Reset");
        move.addActionListener(e -> {
        	this.logic.move();
        	IntStream.range(0, size).forEach(i -> jbs.get(i).setText(" "));
        	this.jbs.get(this.logic.getCurrent()).setText("*");
        });
        reset.addActionListener(e -> {
        	this.logic.reset();
        	IntStream.range(0, size).forEach(i -> jbs.get(i).setText(" "));
        	IntStream.range(0, size).forEach(i -> jbs.get(i).setEnabled(true));
        	this.jbs.get(this.logic.getCurrent()).setText("*");
        });
        
        jp.add(move);
        jp.add(reset);
        JFrame jf = new JFrame();
        jf.getContentPane().add(jp);
        jf.pack();
        jf.setVisible(true);
    }

}
