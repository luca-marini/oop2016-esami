package a02a.e2;

public interface Logics {

	void move ();
	
	void moveRight();
	
	void moveLeft();
	
	void hit(int pos);
	
	boolean isEnabled(int pos);
	
	void reset();
	
	int getCurrent();
}
